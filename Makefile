ALLENNLP=allennlp
CONFIG_FILE=config/lstm_classifier.jsonnet
SERIARIZATION_DIR=dumped


all: clean train

train:
	$(ALLENNLP) train -s $(SERIARIZATION_DIR) $(CONFIG_FILE)

clean:
	rm -rf $(SERIARIZATION_DIR)
