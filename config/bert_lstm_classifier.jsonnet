local embedding_dim = 768;
local hidden_dim = 5;

{
    "dataset_reader": {
        "lazy": false,
        "type": "text_classification_json",
        "tokenizer": {
            "word_splitter": "spacy"
        },
        "token_indexers": {
            "bert": {
                "type": "bert-pretrained",
                "pretrained_model": "https://s3.amazonaws.com/models.huggingface.co/bert/bert-base-uncased-vocab.txt",
                "do_lowercase": true,
                "use_starting_offsets": true
            },
        },
        "max_sequence_length": 400
    },
    "train_data_path": "data/integer.jsonl",
    "validation_data_path": "data/integer.jsonl",
    "model": {
        "type": "basic_classifier",
        "text_field_embedder": {
            "allow_unmatched_keys": true,
            "embedder_to_indexer_map": {
                "bert": ["bert", "bert-offsets"],
                "token_characters": ["token_characters"],
            },
            "token_embedders": {
                "bert": {
                    "type": "bert-pretrained",
                    "pretrained_model": "https://s3.amazonaws.com/models.huggingface.co/bert/bert-base-uncased.tar.gz"
                },
            }
        },
        "seq2vec_encoder": {
           "type": "lstm",
           "input_size": embedding_dim,
           "hidden_size": hidden_dim
        }
    },
    "iterator": {
        "type": "bucket",
        "sorting_keys": [["tokens", "num_tokens"]],
        "batch_size": 5
    },
    "trainer": {
        "optimizer": {
            "type": "adam",
            "lr": 0.001
        },
        "validation_metric": "+accuracy",
        "num_serialized_models_to_keep": 1,
        "num_epochs": 3,
        "grad_norm": 10.0,
        "patience": 5,
        "cuda_device": -1
    }
}
